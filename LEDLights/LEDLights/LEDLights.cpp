#define F_CPU 1000000UL // 8Mhz with ckdiv8

// 25 = 2400 @ 1Mhz
// 6 = 9600 @ 1Mhz
// 2 = 19.2k @ 1Mhz
// 0 = 57.6k @ 1Mhz
#define BAUDRATE 2UL

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

volatile uint8_t leds[10];
volatile uint8_t cnt = 0;

inline void update()
{
	uint8_t porta = 0;
	uint8_t portb = 0;
	uint8_t portd = 0;
	
	if (leds[0] > cnt)
	{
		porta |= (1<<PINA0);
	}
	if (leds[1] > cnt)
	{
		porta |= (1<<PINA1);
	}
	if (leds[2] > cnt)
	{
		portb |= (1<<PINB0);
	}
	if (leds[3] > cnt)
	{
		portb |= (1<<PINB1);
	}
	if (leds[4] > cnt)
	{
		portb |= (1<<PINB2);
	}
	if (leds[5] > cnt)
	{
		portb |= (1<<PINB3);
	}
	if (leds[6] > cnt)
	{
		portb |= (1<<PINB4);
	}
	if (leds[7] > cnt)
	{
		portd |= (1<<PIND2);
	}
	if (leds[8] > cnt)
	{
		portd |= (1<<PIND3);
	}
	if (leds[9] > cnt)
	{
		portd |= (1<<PIND4);
	}
	PORTA = porta;
	PORTB = portb;
	PORTD = portd;
	
	++cnt;
	if (cnt > 0x0D)
	{
		cnt = 0;
	}
}

volatile bool updating = false;

ISR(TIMER0_OVF_vect)
{
	TCNT0 = 180;
	if (updating)
	{
		return;
	}
	updating = true;
	update();
	updating = false;
}

inline void set_led(uint8_t idx, uint8_t strength)
{
	leds[idx] = strength;
}

ISR(USART_RX_vect)
{
	uint8_t data = UDR;
	
	if (data < 0x10 && data > 0xAF)
	{
		// Lamps are numbered from 1 to 10 (decimal)
		return;
	}
	
	uint8_t lamp = (data >> 4) - 1;
	uint8_t strength = data & 0x0F;
	set_led(lamp, strength);
}

void set_uart_8bit()
{
	UCSRC |= (1<<UCSZ0)|(1<<UCSZ1);
}

void enableOverflowTimer0()
{
	TIMSK |= (1 << TOIE0);
	TCCR0B |= (1 << CS01); // clock/8
}

void allLightsOff()
{
	for (uint8_t i = 0; i < 10; ++i)
	{
		set_led(i, 0);
	}
}

void setAll(uint8_t strength)
{
	for (uint8_t i = 0; i < 10; ++i)
	{
		set_led(i, strength);
	}
}

int main(void)
{
	DDRA = 0xFF;
	DDRB = 0xFF;
	DDRD = 0xFF;
	PORTA = 0;
	PORTB = 0;
	PORTD = 0;
	
	enableOverflowTimer0();
	
	UBRRH = (uint8_t)(BAUDRATE>>8);
	UBRRL = (uint8_t)BAUDRATE;
	UCSRB |= (1<<RXEN)|(1<<RXCIE);
	
	sei();
	
	for (uint8_t i = 0; i < 10; ++i)
	{
		allLightsOff();
		set_led(i, 0xF);
		_delay_ms(500);
	}
	allLightsOff();
	for (uint8_t i = 0; i <= 0xF; ++i)
	{
		setAll(i);
		_delay_ms(500);
	}
	
	allLightsOff();
	
	while(1) {}

}