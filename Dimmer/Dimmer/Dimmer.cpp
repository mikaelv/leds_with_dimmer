#define F_CPU 1000000UL // 8Mhz with ckdiv8

// 25 = 2400 @ 1Mhz
// 6 = 9600 @ 1Mhz
// 2 = 19.2k @ 1Mhz
// 0 = 57.6k @ 1Mhz
#define BAUDRATE 2UL

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

void uart_transmit(uint8_t data)
{
	while (!( UCSR0A & (1<<UDRE0))); // wait until register is free
	UDR0 = data;
}

void set_uart_8bit()
{
	UCSR0C |= (1<<UCSZ00)|(1<<UCSZ01);
}

void reset()
{
	uart_transmit(0x10);
	uart_transmit(0x20);
	uart_transmit(0x30);
	uart_transmit(0x40);
	uart_transmit(0x50);
	uart_transmit(0x60);
	uart_transmit(0x70);
	uart_transmit(0x80);
	uart_transmit(0x90);
	uart_transmit(0xA0);
}

volatile uint8_t adc0 = 0;
volatile uint8_t adc1 = 1;

inline uint8_t get_dimmer_value()
{
	return (0xFF - adc0) >> 4;
}

void send_normal()
{
	uint8_t dimmer_value = get_dimmer_value();
	for (int lamp = 1; lamp <= 10; ++lamp)
	{
		uint8_t lamp8 = lamp;
		uint8_t cmd = lamp8 << 4 | dimmer_value;
		uart_transmit(cmd);
	}
}

volatile uint8_t current_lamp = 1;
void send_one_at_the_time()
{
	if (current_lamp > 10)
	{
		current_lamp = 1;
	}
	uint8_t dimmer_value = get_dimmer_value();
	for (uint8_t lamp = 1; lamp <= 10; ++lamp)
	{
		if (lamp == current_lamp)
		{
			uart_transmit(lamp << 4 | dimmer_value);
		}
		else
		{
			uart_transmit(lamp << 4);
		}
	}
	current_lamp++;
}

volatile uint8_t current_bounce = 0;
void send_bounce()
{
	if (current_bounce > 17)
	{
		current_bounce = 0;
	}
	for (uint8_t lamp = 1; lamp <= 10; ++lamp)
	{
		if (current_bounce <= 9)
		{
			if (lamp - 1 == current_bounce)
			{
				uart_transmit(lamp << 4 | 0xF);
			}
			else if (lamp - 1 == current_bounce - 1)
			{
				
				uart_transmit(lamp << 4 | 0x3);
			}
			else if (lamp - 1 == current_bounce - 2)
			{
							
				uart_transmit(lamp << 4 | 0x1);
			}
			else
			{
				uart_transmit(lamp << 4);
			}
		}
		else
		{
			if (lamp - 1 == 18 - current_bounce)
			{
				uart_transmit(lamp << 4 | 0xF);
			}
			else if (lamp - 1 == 19 - current_bounce)
			{
				uart_transmit(lamp << 4 | 0x3);
			}
			else if (lamp - 1 == 20 - current_bounce)
			{
				uart_transmit(lamp << 4 | 0x1);
			}
			else
			{
				uart_transmit(lamp << 4);
			}
		}
	}
	current_bounce++;
}

void send()
{
	if (adc1 < 50)
	{
		send_normal();
		_delay_ms(50);
	}
	else if (adc1 < 100)
	{
		send_one_at_the_time();
		_delay_ms(100);
	}
	else if (adc1 < 150)
	{
		send_one_at_the_time();
		_delay_ms(50);
	}
	else if (adc1 < 200)
	{
		send_bounce();
		_delay_ms(50);
	}
	else
	{
		reset();
	}
}

volatile bool current_adc = 0;

ISR(ADC_vect)
{
	ADMUX &= 0b11110000;
	if (current_adc == 0)
	{
		adc0 = ADCH;
		current_adc = 1;
		ADMUX |= 0b00000001;
	}
	else
	{
		adc1 = ADCH;
		current_adc = 0;
		// ADMUX |= 0b00000000;
	}
	ADCSRA |= (1<<ADSC);
}

int main(void)
{
	UBRR0H = (uint8_t)(BAUDRATE>>8);
	UBRR0L = (uint8_t)BAUDRATE;
	UCSR0B |= (1<<TXEN0);
	
	set_uart_8bit();
	reset();
	
	ADMUX = (1<<REFS0) | (1<<ADLAR);
	ADCSRA = (1<<ADEN) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2) | (1<<ADIE) | (1<<ADSC);
	
	sei();
	
	while(1)
	{
		send();
	}
}